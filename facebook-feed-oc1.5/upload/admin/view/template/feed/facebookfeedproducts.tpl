<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/feed.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <table class="form">
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="facebookfeedproducts_status">
                <?php if ($facebookfeedproducts_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          
          <tr>
            <td>Currency</td>
            <td>
            <select name="currency">
           <?php foreach ($currencies as $curr) { ?>
            <option value="<?php echo $curr['code']; ?>" <?php if($curr['code']== $currency) echo 'selected="selected"';?> ><?php echo $curr['title']; ?></option>
           <?php } ?>
            </select>
            </td>
          </tr>
          
           <tr>
            <td>Language</td>
            <td>
            <select name="language">
           <?php foreach ($languages as $lang) { ?>
            <option value="<?php echo $lang['code']; ?>" <?php if($lang['code']== $language) echo 'selected="selected"';?> ><?php echo $lang['name']; ?></option>
           <?php } ?>
            </select>
            </td>
          </tr>
           <tr>
            <td>Color</td>
            <td>
            <select name="color">
			<option value="">Select options for Color</option>
           <?php foreach ($options as $opt) { ?>
            <option value="<?php echo $opt['name']; ?>" <?php if($opt['name']== $color) echo 'selected="selected"';?> ><?php echo $opt['name']; ?></option>
           <?php } ?>
            </select>
            </td>
          </tr>
		    <tr>
            <td>Size</td>
            <td>
            <select name="size">
			<option value="">Select options for Size</option>
           <?php foreach ($options as $opt) { ?>
            <option value="<?php echo $opt['name']; ?>" <?php if($opt['name']== $size) echo 'selected="selected"';?> ><?php echo $opt['name']; ?></option>
           <?php } ?>
            </select>
            </td>
          </tr>
		    <tr>
            <td>Pattern</td>
            <td>
            <select name="pattern">
			<option value="">Select options for Pattern</option>
           <?php foreach ($options as $opt) { ?>
            <option value="<?php echo $opt['name']; ?>" <?php if($opt['name']== $pattern) echo 'selected="selected"';?> ><?php echo $opt['name']; ?></option>
           <?php } ?>
            </select>
            </td>
          </tr>
		    <tr>
            <td>Material</td>
            <td>
            <select name="material">
			<option value="">Select options for Material</option>
           <?php foreach ($options as $opt) { ?>
            <option value="<?php echo $opt['name']; ?>" <?php if($opt['name']== $material) echo 'selected="selected"';?> ><?php echo $opt['name']; ?></option>
           <?php } ?>
            </select>
            </td>
          </tr>
		  
          
          
        </table>
      </form>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <table class="form">
           <tr>
            <td><?php echo $entry_google_product_category; ?>
            
            <a onclick="$('#form_ub').click();" class="button">Update</a>
            <input type='submit' name="form_ub" id="form_ub" hidden>
             </td>
            <td><textarea cols="100" rows="1" name="google_product_category_url"></textarea>
            <p>Example:<i>https://www.google.com/basepages/producttype/taxonomy-with-ids.en-US.txt</i></p>
            </td>
            
          </tr>
        </table>
      </form>
        <?php if (isset($googlemaincategorys)) { ?>  
              <table class="form">
             <?php if ($categories) { ?>
            <?php foreach ($categories as $category) { $cat =''; ?>
            <tr>
              <td class="left"><?php echo $category['name']; ?></td> 
              <td class="left">
            
              <select name="googlemaincategory_<?php echo $category['category_id']; ?>" data-int="<?php echo $category['category_id']; ?>" class="form-control">
            <option value="" >Select...</option>
           <?php foreach ($googlemaincategorys as $googlemaincategory) { ?>
            <option value="<?php echo $googlemaincategory['google_category_id']; ?>" <?php if($googlemaincategory['google_category_id']== $category['google_category_id']) { echo 'selected="selected"'; $cat = $googlemaincategory['google_category_id']; }?> ><?php echo $googlemaincategory['google_category_name']; ?></option>
           <?php } ?>
            </select>
            <input type='text'  style="width: calc(100% - 200px);" name="googlecategory_<?php echo $category['category_id']; ?>" data-int="<?php if ($cat) echo $cat; ?>" data-category_id="<?php if ($category['category_id']) echo $category['category_id']; ?>" value="<?php echo $category['google_category_name']; ?>" <?php if (!$cat) echo 'disabled'; ?> >
            </td>

            </tr>
            <?php } ?>
            <?php } ?>
            
        </table>
        <?php } ?>
      
      
       <table style="width: 100%;">
      <tr>
            <td><?php echo $entry_data_feed; ?></td>
            <td><a href="<?php echo $data_feed; ?>" target="_blank"  class="button">Download</a></td>
          </tr>
         </table> 
    </div>
  </div>
</div>
<?php echo $footer; ?>
<style>
.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    cursor: not-allowed;
    background-color: #eee;
    opacity: 1;
}
</style>
<script>


$('select[name^=googlemaincategory]').change(function() {
    var button = $(this);
    var google_main_category_id = $('option:selected',this).val();
    console.log($('option:selected',this).val());
    var category_id = button.attr('data-int');
    button.prop('disabled', true);
			$.ajax({ 
			   type: 'GET', 
			   url: 'index.php?route=feed/facebookfeedproducts/savegooglecat&token=<?php echo $token; ?>&google_main_category_id='+google_main_category_id+'&category_id='+category_id,
			   dataType: 'json', 
		       success: function(){
               },
                complete: function() { 
                button.prop('disabled', false);
                $('input[name=googlecategory_'+category_id+']').val('');
                $('input[name=googlecategory_'+category_id+']').attr('data-int', google_main_category_id);
                $('input[name=googlecategory_'+category_id+']').prop('disabled', false);
                if (google_main_category_id  == "") {
                   $('input[name=googlecategory_'+category_id+']').prop('disabled', true); 
                }
                }
		     });
    
 
});
var name;
var category_id;
$('input[name^=googlecategory]').autocomplete({
	minLength : 3,
	source: function(request, response) {
        var filter_id = $(this.element).attr("data-int");
        name = $(this.element).prop("name");
        category_id = $(this.element).attr("data-category_id");
		$.ajax({
			url: 'index.php?route=feed/facebookfeedproducts/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_id='  + filter_id,
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.google_category_name,
						value: item.google_category_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
            $.ajax({ 
			   type: 'GET', 
			   url: 'index.php?route=feed/facebookfeedproducts/savegooglecat&token=<?php echo $token; ?>&google_category_id='+ui.item.value+'&category_id='+category_id,
			   dataType: 'json', 
		       success: function(){
               },
               complete: function() { 
               }
		     });
        $('input[name='+name+']').attr('value', ui.item.label);
	
		return false;
	},
	focus: function(event, ui) {
      return false;
   }
});

</script>
<?php 
class ControllerFeedFacebookFeedProducts extends Controller {
	private $error = array(); 
	
	public function index() {
		$this->load->language('feed/facebookfeedproducts');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		$this->load->model('catalog/option');
		$this->load->model('feed/facebookfeedproducts');
        $this->data['success'] = '';

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate() && !$this->request->post['form_ub']) {
			$this->model_setting_setting->editSetting('facebookfeedproducts', $this->request->post);				
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'));
		} elseif (isset($this->request->post['google_product_category_url']) && $this->request->post['google_product_category_url'] !='' )  {
            $data = array();
            
            if ($file = file($this->request->post['google_product_category_url'])){
                
                $this->model_feed_facebookfeedproducts->clearGoogleCategorys();
                $count=0;   
              foreach ($file as $f) {  

                if (!strstr($f, '#')){
                    $buffer = explode("-", $f);
                    $google_category_name = trim($buffer[1]);
                    $google_category_id = trim($buffer[0]);
                    $google_category_parent = 0;
                    if (count($buffer1= explode(">", $buffer[1]))>1){
                    //$google_category_name = trim($buffer1[count($buffer1)-1]); 
                    $parent = $this->model_feed_facebookfeedproducts->getGoogleCategoryByName(trim($buffer1[0]));
                    $google_category_parent = $parent["google_category_id"];
                               
                }
                    
                $data  = array('google_category_id'=> $google_category_id, 'google_category_name'=>$google_category_name, 'google_category_parent'=>$google_category_parent); 
                $this->model_feed_facebookfeedproducts->updateGoogleCategorys($data);
                $count++; 
                }
           
              }
              $this->data['success']="The category list has been updated. Loaded $count categories."; 
            } else {
              $this->error['warning']='File not found';  
            }
        }
        $this->data['token'] = $this->session->data['token'];
        
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_data_feed'] = $this->language->get('entry_data_feed');
        
		$this->data['entry_google_product_category'] = $this->language->get('entry_google_product_category');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_feed'),
			'href'      => $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('feed/facebookfeedproducts', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		
		$data_opt= array();
		$opt = $this->model_catalog_option->getOptions($data_opt);

		foreach ($opt as $op) {

			$this->data['options'][] = array(
				'name'       => $op['name']
			);
		}
		
		
		$this->data['action'] = $this->url->link('feed/facebookfeedproducts', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['facebookfeedproducts_status'])) {
			$this->data['facebookfeedproducts_status'] = $this->request->post['facebookfeedproducts_status'];
			$this->data['currency'] = $this->request->post['currency'];
			$this->data['language'] = $this->request->post['language'];
			$this->data['size'] = $this->request->post['size'];
			$this->data['color'] = $this->request->post['color'];
			$this->data['pattern'] = $this->request->post['pattern'];
			$this->data['material'] = $this->request->post['material'];
		} else {
			$this->data['facebookfeedproducts_status'] = $this->config->get('facebookfeedproducts_status');
			$this->data['currency'] = $this->config->get('currency');
			$this->data['language'] = $this->config->get('language');
			$this->data['color'] = $this->config->get('color');
			$this->data['size'] = $this->config->get('size');
			$this->data['pattern'] = $this->config->get('pattern');
			$this->data['material'] = $this->config->get('material');
		}
		
        $this->load->model('catalog/category');
        $data = '';
        $results = $this->model_catalog_category->getCategories($data);

		foreach ($results as $result) {
        $google_category_name ='';
        $google_category_id = '';
        $google_category_id = $this->model_feed_facebookfeedproducts->getGoogleCategorysByCategoryId($result['category_id']);
        if ($google_category_id){
            $google_category_name = $this->model_feed_facebookfeedproducts->getGoogleCategoryById($google_category_id['google_category_id']);    
        }

            $this->data['categories'][] = array(
				'category_id' => $result['category_id'],
				'google_category_id' => ($google_category_id) ? $google_category_id["google_main_category_id"]:'',
				'google_category_name' => ($google_category_name) ? $google_category_name["google_category_name"]:'',
				'name'        => $result['name']
            );
		}
        $results = $this->model_feed_facebookfeedproducts->getGoogleMainCategory();

        foreach ($results as $result) {

			$this->data['googlemaincategorys'][] = array(
				'google_category_id'    => $result['google_category_id'],
				'google_category_name'  => $result['google_category_name']
				);
		} 
        
        
		$this->load->model('localisation/currency');
		$data=array();
		$currencies = $this->model_localisation_currency->getCurrencies($data);
		foreach ($currencies as $result) {
				
			$this->data['currencies'][] = array(
				'currency_id'   => $result['currency_id'],
				'title'         => $result['title'],
				'code'          => $result['code'],
				'value'         => $result['value']								
			);
		}	
		
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages($data);

		foreach ($languages as $result) {
								
			$this->data['languages'][] = array(
				'language_id' => $result['language_id'],
				'name'        => $result['name'] ,
				'code'        => $result['code']				
			);		
		}
		$this->data['data_feed'] = HTTP_CATALOG . 'index.php?route=feed/facebookfeedproducts';

		$this->template = 'feed/facebookfeedproducts.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	} 
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'feed/facebookfeedproducts')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
    
	public function install() {
		$this->load->model('feed/facebookfeedproducts');
		$this->model_feed_facebookfeedproducts->install();
        $file = file('https://www.google.com/basepages/producttype/taxonomy-with-ids.en-US.txt');
        foreach ($file as $f) {  

            if (!strstr($f, '#')){
                $buffer = explode("-", $f);
                $google_category_name = trim($buffer[1]);
                $google_category_id = trim($buffer[0]);
                $google_category_parent = 0;
                if (count($buffer1= explode(">", $buffer[1]))>1){
                    $parent = $this->model_feed_facebookfeedproducts->getGoogleCategoryByName(trim($buffer1[0]));
                    $google_category_parent = $parent["google_category_id"];
                }
                    
                $data  = array('google_category_id'=> $google_category_id, 'google_category_name'=>$google_category_name, 'google_category_parent'=>$google_category_parent); 
                $this->model_feed_facebookfeedproducts->updateGoogleCategorys($data);
            
            }
           
        }
        
	}

	public function uninstall() {
		$this->load->model('feed/facebookfeedproducts');
		$this->model_feed_facebookfeedproducts->uninstall();
	}
    
	public function savegooglecat() {
		
        if ($this->request->server['REQUEST_METHOD'] == 'GET'){
            $this->load->model('feed/facebookfeedproducts');
            if (isset($this->request->get["google_main_category_id"])) {
                $data = array('category_id'=>$this->request->get["category_id"] , 'google_main_category_id'=> $this->request->get['google_main_category_id'], 'google_category_id'=>NULL);
            }else{
                $data = array('category_id'=>$this->request->get["category_id"] , 'google_main_category_id'=> NULL, 'google_category_id'=>$this->request->get['google_category_id']);
            }
            $data = 
            $this->model_feed_facebookfeedproducts->saveGoogleCategory($data);
            
        }
        
	}
	
    public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
		$this->load->model('feed/facebookfeedproducts');

			$data = array(
				'google_category_name' => $this->request->get['filter_name'],
				'google_category_id'       => $this->request->get['filter_id']
			);


		$results = $this->model_feed_facebookfeedproducts->getGoogleCategorys($data);
			foreach ($results as $result) {
				$json[] = array(
					'google_category_id' => $result['google_category_id'], 
					'google_category_name'            => strip_tags(html_entity_decode($result['google_category_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$this->response->setOutput(json_encode($json));
    }
}
?>
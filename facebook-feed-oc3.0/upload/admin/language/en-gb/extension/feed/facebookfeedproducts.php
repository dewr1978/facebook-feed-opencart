<?php
// Heading
$_['heading_title']    = 'Facebook Feed Products';

// Text   
$_['text_feed']        = 'Facebook Feed Products';
$_['text_success']     = 'uccess: You have modified Facebook feed!';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_data_feed']  = 'Data Feed Url:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Facebook feed!';
?>
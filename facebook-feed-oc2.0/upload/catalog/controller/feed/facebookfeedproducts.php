<?php 

	
class ControllerFeedFacebookFeedProducts extends Controller {

	public function index() {

		if ($this->config->get('facebookfeedproducts_status')) 
		{ 

			$output  = '<?xml version="1.0" encoding="UTF-8" ?>'. "\n";

			$output .= '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">'. "\n";

            $output .= '<channel>'. "\n";

			$output .= '<title>' . $this->config->get('config_name') . '</title>'. "\n"; 

			$output .= '<description>' . $this->config->get('config_meta_description') . '</description>'. "\n";

			$output .= '<link>' . HTTP_SERVER . '</link>'. "\n";


		

			

			$this->load->model('catalog/category');

			

			$this->load->model('catalog/product');

			

			$this->load->model('tool/image');

			$data=array();

			if(isset($this->request->get['language']) && $this->request->get['language']!=$this->session->data['language'] )
			{
				$this->session->data['language'] = $this->request->get['language'];				
				$this->response->redirect($_SERVER['REQUEST_URI']);
			}
			else if (($this->config->get('facebookfeedproducts_language')!=$this->session->data['language']) && !isset($this->request->get['language']) ) { 
				$this->session->data['language']=$this->config->get('facebookfeedproducts_language');
				$this->response->redirect($_SERVER['REQUEST_URI']);
			}

			$products = $this->model_catalog_product->getProducts($data);
			foreach ($products as $product) {
				$price = $product['price'];
				$product_name = $product['name'];
				$product_id = $product['product_id'];
				$product_link = $this->url->link('product/product', 'product_id=' . $product['product_id']);
				$product_quantity = 0;

				if ($product['description']) {
					
					if ($optionss = $this->model_catalog_product->getProductOptions($product['product_id'])) {

						$option = array();
						$options = array();
						
						for ($i = 0; $i < count($optionss); $i++) {

							if ($optionss[$i]['name'] == $this->config->get('facebookfeedproducts_color') || $optionss[$i]['name'] == $this->config->get('facebookfeedproducts_size') || $optionss[$i]['name'] == $this->config->get('facebookfeedproducts_pattern') || $optionss[$i]['name'] == $this->config->get('facebookfeedproducts_material')){
							$options[]=$optionss[$i];
							}
						
						}

			
					if (isset($options[0])) {
						foreach ($options[0]['product_option_value'] as $option0) {
								if ($option0['price_prefix'] == "+"){
									$price0 = $price + $option0['price'];	
								} else{
									$price0 = $price - $option0['price'];	
								}										
								
								$product_name0 = $product_name . " " . $option0['name'];
								$product_id0 = $product_id . '-' . $option0['product_option_value_id'];
								$product_link0 = $product_link . '&amp;' . $option0['name'];
								$product_quantity = $product_quantity + $option0['quantity'];
								$option[0] = array($options[0]['name'] =>$option0['name']);
							
							if (isset($options[1]) ) {
								foreach ($options[1]['product_option_value'] as $option1) {
									if ($option1['price_prefix'] == "+"){
										$price1 = $price0 + $option1['price'];	
									} else{
										$price1 = $price0 - $option1['price'];	
									}										
										
									$product_name1 = $product_name0 . " " . $option1['name'];
									$product_id1 = $product_id0 . '-' . $option1['product_option_value_id'];
									$product_link1 = $product_link0 . '&amp;' . $option1['name'];
									$product_quantity = $product_quantity + $option1['quantity'];
									$option[1] = array($options[1]['name'] =>$option1['name']);

								
								if (isset($options[2]) ) {

									foreach ($options[2]['product_option_value'] as $option2) {
										if ($option2['price_prefix'] == "+"){
											$price2 = $price1 + $option2['price'];	
										} else{
											$price2 = $price1 - $option2['price'];	
										}										
										
										$product_name2 = $product_name1 . " " . $option2['name'];
										$product_id2 = $product_id1 . '-' . $option2['product_option_value_id'];
										$product_link2 = $product_link1 . '&amp;' . $option2['name'];
										$product_quantity = $product_quantity + $option2['quantity'];
										$option[2]= array($options[2]['name'] =>$option2['name']);	

									
									if (isset($options[3])) {

										$data = array();
										foreach ($options[3]['product_option_value'] as $option3) {
											
											if ($option3['price_prefix'] == "+"){
												$price3 = $price2 + $option3['price'];	
											} else{
												$price3 = $price2 - $option3['price'];	
											}
											$product_name3 = $product_name2 . " " . $option3['name'];
											$product_id3 = $product_id2 . '-' . $option3['product_option_value_id'];
											$product_link3 = $product_link2 . '&amp;' . $option3['name'];
											$product_quantity = $product_quantity + $option3['quantity'];
											$option[3] = array($options[3]['name'] =>$option3['name']);
											
											$data = array(
											'title'	=>	$product_name3,
											'link'	=>	$product_link3,
											'id'	=>	$product_id3,
											'product_id'	=>	$product['product_id'],
											'description'	=>	htmlspecialchars($product['description']),
											'brand'	=>	html_entity_decode($product['manufacturer'], ENT_QUOTES, 'UTF-8'),
											'image'	=>	$product['image'],
											'mpn'	=>	$product['model'],
											'special'	=>	$product['special'],
											'price'	=>	$price,
											'tax_class_id'	=>	$product['tax_class_id'],
											'option'	=>	 $option,
											'quantity'	=>	$product_quantity / 4
											);

											$output .= $this->getItem($data);

															
										}

									} else {
										$data = array();
												
												$data = array(
												'title'	=>	$product_name2,
												'link'	=>	$product_link2,
												'id'	=>	$product_id2,
												'product_id'	=>	$product['product_id'],
												'description'	=>	htmlspecialchars($product['description']),
												'brand'	=>	html_entity_decode($product['manufacturer'], ENT_QUOTES, 'UTF-8'),
												'image'	=>	$product['image'],
												'mpn'	=>	$product['model'],
												'special'	=>	$product['special'],
												'price'	=>	$price,
												'tax_class_id'	=>	$product['tax_class_id'],
												'option'	=>	 $option,
												'quantity'	=>	$product_quantity / 3
												);
												
												$output .= $this->getItem($data);
																
										}
									}	 
								}else {
										$data = array();
												$data = array(
												'title'	=>	$product_name1,
												'link'	=>	$product_link1,
												'id'	=>	$product_id1,
												'product_id'	=>	$product['product_id'],
												'description'	=>	htmlspecialchars($product['description']),
												'brand'	=>	html_entity_decode($product['manufacturer'], ENT_QUOTES, 'UTF-8'),
												'image'	=>	$product['image'],
												'mpn'	=>	$product['model'],
												'special'	=>	$product['special'],
												'price'	=>	$price,
												'tax_class_id'	=>	$product['tax_class_id'],
												'option'	=>	 $option,
												'quantity'	=>	$product_quantity / 2
												);
												
												$output .= $this->getItem($data);

								}
								
							}
							}else {
										$data = array();
												$data = array(
												'title'	=>	$product_name0,
												'link'	=>	$product_link0,
												'id'	=>	$product_id0,
												'product_id'	=>	$product['product_id'],
												'description'	=>	htmlspecialchars($product['description']),
												'brand'	=>	html_entity_decode($product['manufacturer'], ENT_QUOTES, 'UTF-8'),
												'image'	=>	$product['image'],
												'mpn'	=>	$product['model'],
												'special'	=>	$product['special'],
												'price'	=>	$price,
												'tax_class_id'	=>	$product['tax_class_id'],
												'option'	=>	 $option,
												'quantity'	=>	$product_quantity
												);
												
												$output .= $this->getItem($data);

								}
							}	
						}
						
					} else {
					$data = array(
					'title'	=>	$product_name,
					'link'	=>	$product_link,
					'id'	=>	$product_id,
					'product_id'	=>	$product['product_id'],
					'description'	=>	htmlspecialchars($product['description']),
					'brand'	=>	html_entity_decode($product['manufacturer'], ENT_QUOTES, 'UTF-8'),
					'image'	=>	$product['image'],
					'mpn'	=>	$product['model'],
					'special'	=>	$product['special'],
					'price'	=>	$price,
					'tax_class_id'	=>	$product['tax_class_id'],
					'quantity'	=>	$product['quantity']
					);

					$output .= $this->getItem($data);
					
				}
			}
		}
		
			
			$output .= '</channel>'. "\n"; 
			$output .= '</rss>'. "\n";	
			
			
			$this->response->addHeader('Content-type: text/xml');
			$this->response->setOutput($output);
			
			
		}
	}

	protected function getItem($data) {

			if(isset($this->request->get['currency'])!='')
			{
				$currency=$this->request->get['currency'];
			}
			else if ($this->config->get('facebookfeedproducts_currency')!='') { 
				$currency= $this->config->get('facebookfeedproducts_currency');
			}
	
		$output_ = '';
		$output_ .= '<item>'. "\n";
		$output_ .= '<g:title>' . $data['title'] . '</g:title>'. "\n";
		$output_ .= '<g:link>' . $data['link']  . '</g:link>'. "\n";
		$output_ .= '<g:description>' .$data['description'] . '</g:description>'. "\n";
		$output_ .= '<g:brand>' . $data['brand'] . '</g:brand>'. "\n";
		$output_ .= '<g:condition>new</g:condition>'. "\n";
		$output_ .= '<g:id>' . $data['id'] . '</g:id>'. "\n";
															
		if ($data['image']) {
			$output_ .= '<g:image_link>' . $this->model_tool_image->resize($data['image'], 500, 500) . '</g:image_link>'. "\n";
		} else {
			$output_ .= '<g:image_link>' . $this->model_tool_image->resize('no_image.jpg', 500, 500) . '</g:image_link>'. "\n";
		}
															
			$additional_images = $this->model_catalog_product->getProductImages($data['product_id']);
																				
			if($additional_images) {
				foreach ($additional_images as $additional_image) {
					$output_ .= '<g:additional_image_link>'. $this->model_tool_image->resize($additional_image['image'], 500, 500) .'</g:additional_image_link>'. "\n";
				}
			}				

		$output_ .= '<g:mpn>' . $data['mpn'] . '</g:mpn>'. "\n";

		if ((float)$data['special']) {
			$output_ .= '<g:sale_price>' .  $this->currency->format($data['special'], $currency, false, false).' '. $currency . '</g:sale_price>'. "\n";
			$output_ .= '<g:tax_calc_for_sale_price>' .$this->currency->format( $this->tax->getTax($data['special'],$data['tax_class_id']),$currency, false, false) . '</g:tax_calc_for_sale_price>'. "\n";                   
			$output_ .= '<g:taxed_sale_price>' .  $this->currency->format($this->tax->calculate($data['special'], $data['tax_class_id']), $currency, false, false) . '</g:taxed_sale_price>'. "\n";
		}

		$output_ .= '<g:price>' . $this->currency->format($data['price'], $currency, false, false).' '. $currency. '</g:price>'. "\n";
		$output_ .= '<g:tax_calc_for_price>' . $this->currency->format($this->tax->getTax($data['price'],$data['tax_class_id']),$currency, false, false) . '</g:tax_calc_for_price>'. "\n";
		$output_ .= '<g:taxed_price>' . $this->currency->format($this->tax->calculate($data['price'], $data['tax_class_id']), $currency, false, false) . '</g:taxed_price>'. "\n";
															
		$categories = $this->model_catalog_product->getCategories($data['product_id']);
															
			foreach ($categories as $category) {
				$path = $this->getPath($category['category_id']);
																
					if ($path) {
					$string = '';
																	
					foreach (explode('_', $path) as $path_id) {
						$category_info = $this->model_catalog_category->getCategory($path_id);
																		
						if ($category_info) {
							if (!$string) {
								$string = $category_info['name'];
							} else {
								$string .= ' &gt; ' . $category_info['name'];
							}
						}
					}
																 
					$output_ .= '<g:product_type>' . $string . '</g:product_type>'. "\n";
				}
			}				
					   
			$output_ .= '<g:availability>' . ($data['quantity'] ? 'in stock' : 'out of stock') . '</g:availability>'. "\n";
			
			if (isset($data['option'])){
				foreach ($data['option'] as $optio){
					foreach ($optio as $key=>$opt){
						$output_ .= '<g:'.$key.'>' . $opt . '</g:'.$key.'>'. "\n"; 
					}
				}
			}
			
			$output_ .= '</item>'. "\n";
		
		
		return $output_;
	}		

	protected function getPath($parent_id, $current_path = '') {
		$category_info = $this->model_catalog_category->getCategory($parent_id);

		if ($category_info) {
			if (!$current_path) {
				$new_path = $category_info['category_id'];
			} else {
				$new_path = $category_info['category_id'] . '_' . $current_path;
			}

			$path = $this->getPath($category_info['parent_id'], $new_path);

			if ($path) {
				return $path;
			} else {
				return $new_path;
			}
		}
	}	

	



	

}

?>